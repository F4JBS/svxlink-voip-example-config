#!/usr/bin/python3

# Parameters start here

pttkey = 'p'                    # you may change this
path = "/home/damien/.squelch"  # you MUST change this if it doesn't match the value in svxlink.conf

# Parameters end here

from pynput import keyboard

file = open(path, "w")
pttkey_up = keyboard.KeyCode.from_char(pttkey.upper())
pttkey_lo = keyboard.KeyCode.from_char(pttkey.lower())

# definitions independent of implementation
def open_ptt():
        file.write("O")
        file.flush()
def close_ptt():
        file.write("Z")
        file.flush()

def on_press(key):
        if key == pttkey_up or key == pttkey_lo:
                open_ptt()

def on_release(key):
        if key == pttkey_up or key == pttkey_lo:
                close_ptt()

# actually start the program
close_ptt()

try:
        with keyboard.Listener(on_press=on_press, on_release=on_release) as listener:
                listener.join()
except KeyboardInterrupt:
        close_ptt()

file.close()
