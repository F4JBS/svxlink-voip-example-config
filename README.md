# Connect to a SvxLink reflector without a radio

## Description

This guide aims to help you easily connect to a SvxLink reflector from your computer, using your headset/speakers and microphone, and a software PTT.

It was created so that I could have a simple way to stay in touch with fellow OMs without using proprietary software like EchoLink.

This is free software: it is released upon the terms of the GNU General Public License, version 2 or 3. See `COPYING.GPLv2.md` and `COPYING.GPLv3.md`.

Feel free to submit issues, commits and pull requests. Or to make a better version of this. Or just use it.

If you feel especially grumpy, you can even *not* use it, and instead open an issue to complain that this is not radio, that it's abhorrent, that it promotes piracy and bad practices, that everything was better back in the days and that people using that are not real hams anyway. By the way, if you actually feel that way, please check the FAQ.

## Boring stuff (disclaimer et al)

*This section is not legal advice. See the license for more details.*

Please follow local legislation regarding accessing a reflector from a computer, or remotely.

With apologies to many grumpy OMs, operating a VoIP access point connected to physical repeaters transmitting on radio amateur frequencies is legal in most countries, as long as you properly identify just like you would with a proper radio. I do not take responsibility for any misuse. You have the license so you should know what the law is.

Not a licensed amateur radio operator? You can still use this to connect to things like PMR446 or CB nets running SvxReflector, **if allowed by local legislation**. If you break the law bu doing so, that's on you, not on me!

## Requirements

- a device that can run the **Linux** distribution of your choice, on which SvxLink can be installed. Mac and other Unix are not supported, and neither is Windows (feel free to open a pull request if you manage to make it work)
- `alsa`, it's also recommended to either have `pulseaudio` or `pipewire` (check your distribution's package manager to find out if you have one, and how to install one)
	- if you have a desktop distro, you probably already have PulseAudio installed
	- you may try the commands `pulseaudio` and `pipewire` in a terminal to find out whether you're using one or not
- `python3` and the `pynput` module, for my virtual PTT.
	- you may install the keyboard module either from your distro repositories, or from pip: `pip3 install pynput`.
	- if you're using @Electroalex26's version of the virtual PTT, follow his requirements instead.

## Installation

There are 2 things you need to install: the SvxLink config and the virtual PTT.

### SvxLink config

You may install it at `/etc/svxlink/svxlink.conf`. Feel free to tweak it as you please.

**Important:** you need to review the settings for the Tx, Rx and reflector. They are marked with comments (#).

### Virtual PTT

It's a simple Python script that enables transmission of your voice when you press a key, and disables it when you release said key.

You may edit it to change the key. Moreover, the path in the script MUST match the PTY_PATH value in the SvxLink config.

Alternatively, you may use @Electroalex26's https://gitlab.com/Electroalex26/software-ptt-for-svxlink. If you wish to do so, follow the instructions there.

## Usage

Run `svxlink` from your user (not as root).

- If it complains about write permissions, you may either remove the voice mail module from the config, or change ownership of the incriminated directories.

If you're not using @Electroalex26's soft, run the script:

```bash
sudo python3 ./virtual-ptt.py
```

- Unfortunately, the current implementation uses the `keyboard` Python library, and requires root. This may be fixed in a future release. If that bothers you, and if you found a solution, please open a pull request and submit it!

Everything should work fine now. If not, please open an issue so that I can look into it.

## FAQ

### It's not radio! How dare you!

One could argue it's still radio if connected to physical repeaters or access points.

Actually using a radio could be more fun, in my opinion. However, some radioamateurs and radio enthusiasts do not have the budget to set up their hotspot, or even to get a decent radio. Maybe they just don't want to bother.

In my opinion, hams and radio enthusiasts should do whatever they want, as long as it's legal.

### I have an issue!

Please open an issue on this repo. Also, browse the existing and closed issues, maybe someone had the same problem.

I'll try to update this README whenever I feel like something needs to be clarified.

## Credit

Many thanks to:

- @Electroalex26 for the idea, and for helping me with the config
- the SvxLink devs for the software and documentation
